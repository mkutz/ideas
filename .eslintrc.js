module.exports = {
  extends: "eslint:recommended",
  parser: "babel-eslint",
  env: {
    node: true,
    mocha: true,
    browser: true,
    es6: true,
  },
  ecmaFeatures: {
    modules: true,
  },
  rules: {
    "comma-dangle": [2, "always-multiline"],
    "no-undef": [2],
    "quotes": [2, "single"],
    "strict": [2, "never"],
    semi: ["error", "always"],
    "indent": [2, "tab", { SwitchCase: 1 }],
    "no-mixed-spaces-and-tabs": 2,
    "react/jsx-uses-react": 2,
    "react/jsx-uses-vars": 2,
    "react/react-in-jsx-scope": 2,
    "react/no-deprecated": 2,
    "react/jsx-no-undef": 2,
    "jsx-a11y/aria-props": 2,
  },
  plugins: ['flowtype', 'react', 'import', 'jsx-a11y', 'graphql'],
}
