import { configure } from '@kadira/storybook';

function loadStories() {
	require('font-awesome/css/font-awesome.css');
	require('../client/style.css');
	require('glob-loader!./stories.pattern');
}

configure(loadStories, module);
