# Ideas

run `npm run dev` to start developing the client!

## Plop

use `npm run plop` to create stuff for the app, like:

- Components
- Containers (That use state or router)
- Reducers
- Immutable.js Records
