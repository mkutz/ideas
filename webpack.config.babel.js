var path = require('path');
var webpack = require('webpack');
var WebpackNotifier = require('webpack-notifier');
var isProduction = process.env.NODE_ENV && process.env.NODE_ENV.toLowerCase() === 'production';

var webpackConf = {
	devtool: isProduction ? 'source-map' : 'eval-source-map',
	entry: [
		'font-awesome/css/font-awesome.css',
		path.resolve(__dirname, './client/index'),
	],
	progress: true,
	devServer: {
		inline: true,
		hot: true,
	},
	output: {
		path: path.join(__dirname, 'dist'),
		filename: 'bundle.js',
		publicPath: '/dist/',
	},
	plugins: [
		new webpack.DefinePlugin({
			'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV),
		}),
		new webpack.optimize.OccurenceOrderPlugin(),
		new webpack.HotModuleReplacementPlugin(),
		new webpack.NoErrorsPlugin(),
		new WebpackNotifier(),
	],
	module: {
		loaders: [{
			test: /\.js$/,
			loaders: ['babel'],
			include: path.join(__dirname, 'client'),
		}, {
			test: /\.txt$/,
			loaders: ['raw'],
			include: path.join(__dirname, 'client'),
		}, {
			test: /\.css$/,
			loaders: ['style'],
			include: path.join(__dirname, 'node_modules'),
		}, {
			test: /\.css$/,
			loaders: ['style', 'css?modules'],
			include: path.join(__dirname, 'client'),
		}, {
			test: /\.(woff|png|ttf|otf|svg|eot)/,
			loaders: ['file'],
		}, {
			test: /node_modules.+\.css$/,
			loaders: [
				'style', 'css',
			],
		}],
	},
};

if (isProduction) {
	webpackConf.plugins.push(
		new webpack.optimize.UglifyJsPlugin({
			compress: true,
			sourceMap: false,
		})
	);
} else {
	webpackConf.entry.unshift(
		'webpack-hot-middleware/client',
		'webpack/hot/only-dev-server'
	);
}

module.exports = webpackConf;
