// @flow

import React from 'react';
import { Container, VBox, Page, Box } from 'react-layout-components';

type PageLayoutProps = {
	leftComponent?: any;
	centerComponent?: any;
	rightComponent?: any;
	showRight?: boolean;
	showLeft?: boolean;
	showTop?: boolean;
	topComponent?: any;
};

export default function PageLayout({
	leftComponent = <div>nothing here</div>,
	centerComponent = <div style={{ textAlign: 'center', flex: 1 }}>main stuff go here</div>,
	rightComponent = <div>additional data</div>,
	topComponent = <div>Top</div>,
	showRight = false,
	showLeft = true,
	showTop = false,
} : PageLayoutProps) {
	return (
		<Page>
			{ showTop && <Box>{ topComponent }</Box> }
			<Container fit>
				{ showLeft && <Box>{ leftComponent }</Box> }
				<VBox flex="1">
					{ centerComponent }
				</VBox>
				{ showRight && <Box>{ rightComponent }</Box> }
			</Container>
		</Page>
	);
}
