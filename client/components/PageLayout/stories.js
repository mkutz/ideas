// @flow

import React from 'react';
import { storiesOf, action } from '@kadira/storybook';
import PageLayout from './';
import LyricsEditor from '../LyricsEditor';
import { List } from 'immutable';

storiesOf(
	'PageLayout',
	module
).add('with no props', () => (
	<PageLayout />
)).add('with right', () => (
	<PageLayout showRight={ true } />
)).add('without left', () => (
	<PageLayout showRight={ true } showLeft={ false } />
)).add('green div on left', () => (
	<PageLayout leftComponent={ <div style={{ background: 'green', flex: 1  }}>I'm green</div> } />
));
