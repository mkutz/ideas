// @flow

import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import reducers from './reducers';

export default (initialState : any, additionalMiddlewares? : Array<Function>) => {
	const store = createStore(
		reducers,
		initialState,
		compose(
			applyMiddleware(
				thunk,
				...(additionalMiddlewares || [])
			),
			typeof window !== 'undefined' && window.devToolsExtension ? window.devToolsExtension() : f => f
		),
	);

	if (module.hot) {
		module.hot.accept('./reducers', () => {
			const nextRootReducer = require('./reducers');
			store.replaceReducer(nextRootReducer.default);
		});
	}

	return store;
};
