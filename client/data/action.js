// @flow

export type Action = {
	type: string;
	payload: any;
};

export type ThunkedAction = Function | Action;

export type DispatchFn = ( action : ThunkedAction ) => any;

export type GetStateFn = Function;

export type AsyncAction = ( dispatch : DispatchFn, getState : GetStateFn ) => any;
