import Dexie from 'dexie';

let db = new Dexie('Lyrx');
db.version(3).stores({
	friends: '++id,name,age',
});

window.db = db;
