// @flow

import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';

export default combineReducers({
	// inject:reducers
	// inject:end
	routing: routerReducer,
});
