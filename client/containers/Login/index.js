// @flow

import React from 'react';
import { Center } from 'react-layout-components';
import style from './style.css';

export class Login extends React.Component {
	render() {
		return (
			<Center fit className={ style.container }>
				<div className={ style.loginBox }>
					Please login first.
				</div>
			</Center>
		);
	}
}

export default Login;
