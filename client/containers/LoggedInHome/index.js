// @flow

import React from 'react';
import { connect } from 'react-redux';
import PageLayout from '../../components/PageLayout';

type LoggedInHomeProps = {
	right?: any;
	center?: any;
	top?: any;
};

export class LoggedInHome extends React.Component<any, LoggedInHomeProps, any> {
	render() {
		return (
			<PageLayout
				leftComponent={ <div>left</div> }
				centerComponent={ this.props.center || <div>Hey mauwn</div> }
				showRight={ Boolean(this.props.right) }
				rightComponent={ this.props.right }
				showLeft={ false }
				showTop={ Boolean(this.props.top) }
				topComponent={ this.props.top }
			/>
		);
	}
}

const mapState = (state) => {
	return {
	};
};

const mapDispatch = (dispatch) => {
	return {
	};
};

export default connect(
	mapState,
	mapDispatch,
)(LoggedInHome);
