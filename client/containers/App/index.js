// @flow

import React from 'react';
import { Router } from 'react-router';
import { routes } from './routes';
import { Provider } from 'react-redux';

type AppProps = {
	store: any;
	history: any;
};

export default class App extends React.Component<any, AppProps, any> {
	render() {
		return (
			<Provider store={ this.props.store }>
				<Router history={ this.props.history }>
					{ routes }
				</Router>
			</Provider>
		);
	}
}
