// @flow

import React from 'react';
import { Route, IndexRoute, IndexRedirect } from 'react-router';

import WelcomeScreen from '../WelcomeScreen';
import LoggedInHome from '../LoggedInHome';
import CheckLoginMainApp from '../CheckLoginMainApp';
import Login from '../Login';

export const routes = (
	<Route path='/' component={ CheckLoginMainApp }>
		<Route path='/home' component={ LoggedInHome }>
			<IndexRoute components={{ center: WelcomeScreen }} />
		</Route>
		<Route path='/login' component={ Login } />
		<IndexRedirect to='/home' />
	</Route>
);
