// @flow

import React from 'react';
import style from './style.css';
import { VBox } from 'react-layout-components';
import { connect } from 'react-redux';

type WelcomeScreenProps = {
	something?: any;
};

export function WelcomeScreen({ something } : WelcomeScreenProps) {
	return (
		<VBox className={ style.container } center flex="1">
			<h1>Welcome, darling</h1>
			Lets share ideas! { something }
		</VBox>
	);
}

const mapState = state => {
	return {
		something: typeof state,
	};
};

export default connect(mapState)(WelcomeScreen);
