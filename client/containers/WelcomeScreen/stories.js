// @flow

import React from 'react';
import { storiesOf, action } from '@kadira/storybook';
import WelcomeScreen from './';

storiesOf(
	'WelcomeScreen',
	module
).add('with no props', () => (
	<WelcomeScreen />
));
