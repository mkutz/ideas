// @flow

import React from 'react';
import { Page } from 'react-layout-components';

export class CheckLoginMainApp extends React.Component<any, any, void> {
	render() {
		return (
			<Page>
				{ this.props.children }
			</Page>
		);
	}
}

export default CheckLoginMainApp;
