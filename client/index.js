import './style.css';
import React from 'react';
import App from './containers/App';
import { render } from 'react-dom';
import createStore from './data/createStore';
import { browserHistory } from 'react-router';
import { routerMiddleware, syncHistoryWithStore } from 'react-router-redux';

const initialState = {};
const store = createStore(initialState, [
	routerMiddleware(browserHistory),
]);

if (process.env.NODE_ENV !== 'production') {
	store.subscribe(() => {
		console.log(store.getState()); // eslint-disable-line
		window.__dev__currentState = store.getState();
	});
}

const history = syncHistoryWithStore(browserHistory, store);

render(
	<App store={ store } history={ history } />,
	document.getElementById('main')
);

if (module.hot) {
	module.hot.accept('./containers/App/index.js', () => {
		const NewApp = require('./containers/App').default;
		render(<NewApp store={ store } history={ browserHistory } />, document.getElementById('main'));
	});
}
