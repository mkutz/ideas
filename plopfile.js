const componentGenerator = require('./plop/generators/component');
const containerGenerator = require('./plop/generators/container');
const reducerGenerator = require('./plop/generators/reducer');
const recordGenerator = require('./plop/generators/record');

module.exports = plop => {
	plop.setGenerator('component', componentGenerator);
	plop.setGenerator('record', recordGenerator);
	plop.setGenerator('container', containerGenerator);
	plop.setGenerator('reducer', reducerGenerator);
};
