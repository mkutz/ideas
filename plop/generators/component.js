module.exports = {
	description: 'creates a new component',

	prompts: [{
		type: 'input',
		name: 'componentName',
		message: 'What is the component name?',
		validate: function (value) {
			if ((/.+/).test(value)) { return true; }
			return 'component name is required';
		},
	}, {
		type: 'confirm',
		name: 'style',
		message: 'does your component need css?',
	}],

	actions: data => {
		const actions = [{
			type: 'add',
			path: 'client/components/{{ properCase componentName }}/index.js',
			templateFile: './plop/component/index.js.hbs',
		}, {
			type: 'add',
			path: 'client/components/{{ properCase componentName }}/stories.js',
			templateFile: './plop/component/stories.js.hbs',
		}];

		const styleAction = {
			type: 'add',
			path: 'client/components/{{ properCase componentName }}/style.css',
			templateFile: './plop/component/style.css.hbs',
		};

		return !data.style ? actions : actions.concat(styleAction);
	},
};
