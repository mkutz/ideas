module.exports = {
	description: 'creates a new container',

	prompts: [{
		type: 'input',
		name: 'componentName',
		message: 'What is the component name?',
		validate: function (value) {
			if ((/.+/).test(value)) { return true; }
			return 'component name is required';
		},
	}, {
		type: 'confirm',
		name: 'reduxConnect',
		message: 'do you need redux-connect bindings?',
	}, {
		type: 'confirm',
		name: 'style',
		message: 'does your component need css?',
	}],

	actions: data => {
		const actions = [{
			type: 'add',
			path: 'client/containers/{{ properCase componentName }}/index.js',
			templateFile: './plop/container/index.js.hbs',
		}];

		const styleAction = {
			type: 'add',
			path: 'client/containers/{{ properCase componentName }}/style.css',
			templateFile: './plop/container/style.css.hbs',
		};

		return !data.style ? actions : actions.concat(styleAction);
	},
};
