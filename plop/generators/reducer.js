module.exports = {
	description: 'creates a new component',

	prompts: [{
		type: 'input',
		name: 'reducerName',
		message: 'What is the reducer name?',
		validate: function (value) {
			if ((/.+/).test(value)) { return true; }
			return 'reducer name is required';
		},
	}],

	actions: [{
		type: 'add',
		path: 'client/data/{{ camelCase reducerName }}/reducer.js',
		templateFile: './plop/reducer/reducer.js.hbs',
	}, {
		type: 'add',
		path: 'client/data/{{ camelCase reducerName }}/actions.js',
		templateFile: './plop/reducer/actions.js.hbs',
	}, {
		type: 'add',
		path: 'client/data/{{ camelCase reducerName }}/constants.js',
		templateFile: './plop/reducer/constants.js.hbs',
	}],
};
