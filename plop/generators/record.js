module.exports = {
	description: 'creates a new immutable record',

	prompts: [{
		type: 'input',
		name: 'recordName',
		message: 'What is the record name?',
		validate: function (value) {
			if ((/.+/).test(value)) { return true; }
			return 'record name is required';
		},
	}],

	actions: [{
		type: 'add',
		path: 'client/records/{{ properCase recordName }}/index.js',
		templateFile: './plop/record/index.js.hbs',
	}]
};
