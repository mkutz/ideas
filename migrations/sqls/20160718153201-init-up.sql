create table if not exists users (
  id varchar(64) primary key,
);
create table if not exists ideas (
  id serial primary key,
  creator varchar(64) references users(id) not null on delete cascade on update cascade,
  score integer default 0,
  content varchar(1500) not null,
  ending_date timestamp,
  max_votes integer check( max_votes > 0 ),
  closed boolean default false,
  posted timestamp timestamp with time zone not null default now()
);
create table if not exists votes (
  voter varchar(64) references users(id) not null on delete cascade on update cascade,
  idea integer references ideas(id) not null on delete cascade on update cascade,
  score integer check( score <= 1 && score >= -1 ),
  constraint unique_vote_per_user_for_idea primary key (voter, idea)
)

create table if not exists comments (
  id serial primary key,
  commenter varchar(64) references users(id) not null on delete cascade on update cascade,)
