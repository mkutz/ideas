import express from 'express';
import notifier from 'node-notifier';
import serveStatic from 'serve-static';
import webpack from './webpackMiddleware';
import morgan from 'morgan';
import compression from 'compression';

const port = process.env.PORT || 8001;
const app = express();
if (process.env.NODE_ENV === 'production') {
	app.use(compression());
}
app.use(morgan('combined'));
webpack(app);
app.use('/dist', serveStatic(__dirname + '/../dist'));

app.get('/__healthcheck__', (req, res) => {
	res.send('ready');
});

const HTML = `
<body>
<div id="main"></div>
<script src="/dist/bundle.js"></script>
</body>
`;

app.get('/*', (req, res) => res.send(HTML));

app.listen(port, () => {
	console.log(`Started on ${port}. Ctrl-C to stop.`); // eslint-disable-line
	notifier.notify({
		title: 'Toast is up and running',
		message: `Started on ${port}. Ctrl-C to stop.`,
	});
});
