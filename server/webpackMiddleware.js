import webpackConf from '../webpack.config.babel';
import webpack from 'webpack';

const webpackDevMiddleware = require('webpack-dev-middleware');
const webpackHotMiddleware = require('webpack-hot-middleware');
const compiler = webpack(webpackConf);

export default app => {
	if (process.env.NODE_ENV === 'production') return;

	app.use(
		webpackDevMiddleware(compiler, {
			hot: true,
			publicPath: '/dist/',
			stats: {
				colors: true,
			},
			noInfo: false,
			historyApiFallback: true,
		})
	);

	app.use(
		webpackHotMiddleware(compiler, {
			log: console.log, // eslint-disable-line
			path: '/__webpack_hmr',
			heartbeat: 10 * 1000,
		})
	);

};
